//
//  AlamofireManager.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Alamofire
class AlamofireManager: NSObject {
    var httpRequest : HttpObject!
    var restDelegate : RestDelegate!
    init(httpRequest : HttpObject, restDelegate : RestDelegate) {
        self.httpRequest = httpRequest;
        self.restDelegate = restDelegate;
    }
    
    func startDownload(){
        self.restDelegate.onPreExecute(httpRequestObject: httpRequest, forTaskCode: httpRequest.strtaskCode)
        switch httpRequest.methodType {
        case METHODS.GET:
            getData()
            break;
        case METHODS.PUT:
            putData()
            break;
        case METHODS.DELETE:
            deleteData()
            break;
        case METHODS.POST:
            postData()
            break;
        case METHODS.MULTIPART:
            
            break;
        case .POST_JSON:
            postJsonData();
            break;
        }
    }
    
    func getData(){
        let dataRequest : DataRequest = Alamofire.request(httpRequest.strUrl, method: .get, parameters: httpRequest.dicParams, encoding: URLEncoding.default, headers: httpRequest.dicHeaders)
        dataRequest.validate(statusCode: 200..<300).responseString {response in
            print("result:\(response.result.isSuccess)")
            print("value:\(String(describing: response.result.value))")
            switch response.result{
            case .success:
                self.onSuccessResponse(response: response);
                break
            case .failure:
                self.onFailureResponse(response: response);
                break
                
            }
        }
    }
    
//    func onSuccessResponse(response : DataResponse<String>){
//        print("SuccessData:\(String(describing: response.result.value))");
//        let httpResponse = HttpResponse();
//        httpResponse.responseObject = JsonParser.parseString(taskCode: httpRequest.strtaskCode, response: response);
//        httpResponse.responseResult = HttpResponseResult.SUCCESS;
//        let flag = self.restDelegate.onSuccess(httpResponse, forTaskCode: httpRequest.strtaskCode, httpRequestObject: httpRequest);
//        print("Success:\(flag)");
//    }
    
    func onSuccessResponse(response : DataResponse<String>){
        print("SuccessData:\(response.result.value)");
        let httpResponse = HttpResponse();
        httpResponse.responseObject = JsonParser.parseJson(taskCode: httpRequest.strtaskCode, response: response);
        httpResponse.responseResult = HttpResponseResult.SUCCESS;
        let flag = self.restDelegate.onSuccess(httpResponse, forTaskCode: httpRequest.strtaskCode, httpRequestObject: httpRequest);
        print("Success:\(flag)");
    }
    
    
    func onFailureResponse(response : DataResponse<String>){
        print("FailureData:\(response.result.value)");
        let httpResponse = HttpResponse();
        httpResponse.responseResult = HttpResponseResult.FAILURE;
        self.restDelegate.onFailure(httpResponse, forTaskCode: httpRequest.strtaskCode)
    }
    
    
    func postData(){
        let dataRequest : DataRequest = Alamofire.request(httpRequest.strUrl, method: .post, parameters: httpRequest.dicParams, encoding: URLEncoding.default, headers: httpRequest.dicHeaders)
        dataRequest.validate(statusCode: 200..<600).responseString {response in
            print("result:\(response.result.isSuccess)")
            print("value:\(response.result.value)")
            switch response.result{
            case .success:
                self.onSuccessResponse(response: response);
                break
            case .failure:
                self.onFailureResponse(response: response);
                break
                
            }
        }

    }
    
    func postJsonData(){
        let dataRequest : DataRequest = Alamofire.request(httpRequest.strUrl, method: .post, parameters: httpRequest.dicParams, encoding: JSONEncoding.default, headers: httpRequest.dicHeaders)
        dataRequest.validate(statusCode: 200..<300).responseString {response in
            print("result:\(response.result.isSuccess)")
            print("value:\(response.result.value)")
            switch response.result{
            case .success:
                self.onSuccessResponse(response: response);
                break
            case .failure:
                print(response)
                self.onFailureResponse(response: response);
                break
            }
        }
    }
    
    func uploadImageData(imgData : Data, keyName : String, httpRequest : HttpObject, restDelegate : RestDelegate){
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: keyName,fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in httpRequest.dicParams {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:httpRequest.strUrl)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print(response.result.value)
                    let httpResponse = HttpResponse();
                    restDelegate.onSuccess(httpResponse, forTaskCode: TASKCODES.ADD_USER_IMAGE, httpRequestObject: httpRequest)
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
                let httpResponse = HttpResponse();
                restDelegate.onFailure(httpResponse, forTaskCode: TASKCODES.ADD_USER_IMAGE)
            }
        }
    }
    func deleteData(){
        
    }
    
    func putData(){
        
    }
}
