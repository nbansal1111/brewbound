//
//  BaseVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class BaseVC: UIViewController, RestDelegate {
    var delegate : RestDelegate!
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:105, y: 250, width: 80, height: 80), type: .lineSpinFadeLoader, color: UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), padding: 20)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
        activityIndicatorView.center = self.view.center
        activityIndicatorView.isHidden = true;
        self.view.addSubview(activityIndicatorView)
        // Do any additional setup after loading the view.
    }
    
    func imgViewOnRightSide()
    {
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "payemntBag"), style: .plain, target: self, action:#selector(BaseVC.backToView))
    }
    
    

    struct ScreenSize
    {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    }
    
    struct Color {
        // Constant Color define here.
        static let appColor: UIColor = UIColor(red: 238.0/255.0, green: 28.0/255.0, blue: 37.0/255.0, alpha: 1.0)
    }
    
    
//    func getToken() -> String {
//        if let randomString = UserDefaults.standard.value(forKey:"userData") as? NSDictionary{
//            let strtemp = randomString.value(forKey: "token_type") as! String
//            let strtemp1 = randomString.value(forKey: "access_token") as! String
//           print(strtemp1)
//            return strtemp.capitalized + " " + strtemp1;
//        }else{
//            return "";
//        }
//    }
    
    func getToken() -> String {
        let type = UserDefaults.standard.string(forKey: "type")!
        let token = UserDefaults.standard.string(forKey: "logintoken")!
        return type.capitalized + " " + token
    }
    
    func getCustomerId() -> String
    {
       return "94115"
    }

    func downloadData(httpRequest: HttpObject){
        let downloader = AlamofireManager(httpRequest: httpRequest, restDelegate: self);
        downloader.startDownload();
    }
    
    func stopLoader ()
    {
        self.view.isUserInteractionEnabled = true
        
        activityIndicatorView.isHidden = true;
        self.activityIndicatorView.stopAnimating()
    }
    func startLoader ()
    {
        self.view.isUserInteractionEnabled = false
        activityIndicatorView.isHidden = false;
        self.activityIndicatorView.startAnimating()
    }
    func backToView() {
        
        _ = navigationController?.popViewController(animated: true)
    }

    
    // Mark: RestDelegate
    func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        startLoader()
        print("OnPreExecute");
    }
    
    func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        stopLoader()
        print("onSuccess");
        return true;
    }
    
    func onFailure(_ paramObject: HttpResponse, forTaskCode taskCode: TASKCODES) {
        stopLoader()
        print("onFailure");
    }  
   
}

extension UITextField
{
    func setImageLeftSideOnTextfield(image: UIImage)
    {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: self.frame.size.height))
        //imageView.backgroundColor=UIColor.green
        imageView.contentMode = .center
        imageView.image = image
        self.leftView = imageView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func setImageRightSideOnTextfield(image: UIImage)
    {
        let imageViewError = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.height, height: self.frame.size.height))
        imageViewError.contentMode = .right
        imageViewError.image = image
        self.rightView = imageViewError
        self.rightViewMode = UITextFieldViewMode.always
    }
}
