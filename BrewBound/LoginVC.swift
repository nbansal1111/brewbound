//
//  LoginVC.swift
//  BrewBound
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class LoginVC: BaseVC,UITextFieldDelegate
{
    
    @IBOutlet weak var textFldPassword: UITextField!
    @IBOutlet weak var textFldEmail: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        textFldPassword.text = "admin@123"
        textFldEmail.text = "info@byte-matrix.com"
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        for objSubView in self.view.subviews
        {
            if (objSubView is UITextField)
            {
                let textFld: UITextField? = (objSubView as? UITextField)
                textFld?.delegate=self
                let objSpacerView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: (textFld?.frame.size.height)!))
                textFld?.leftView=objSpacerView
                textFld?.leftViewMode=UITextFieldViewMode.always
                textFld?.clipsToBounds=true
            }
        }
    }
    @IBAction func registrationClicked(_ sender: Any) {

            let RegisterVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            self.navigationController?.pushViewController(RegisterVC, animated: true)
    }
    
    @IBAction func btnJoinAct(_ sender: Any)
    {
        
        let request : HttpObject = ApiGenerator.Login(username: textFldEmail.text!, password: textFldPassword.text!)
        downloadData(httpRequest: request);
    }
    
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        print(object.responseObject)
        if flag {
            switch taskcode {
            case TASKCODES.LOGIN:
                let loginRes = object.responseObject as! LoginData
                OnRecievedLoginResponse(loginRes)
                break;
                
            default:
                break;
            }
        }
        return false
    }

    @IBAction func crossBtn(_ sender: Any) {
        backToView()

    }
    
    func OnRecievedLoginResponse(_ loginRes : LoginData){
        print(loginRes)
        let token = loginRes.access_Token
        DefaultsUtil.setUserLoggedIn()
        DefaultsUtil.setUserToken(token!)
        DefaultsUtil.setType(loginRes.tokenType!)
        moveToDashboard()
        
        print(loginRes)
    }

    func moveToDashboard(){
        let objAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        objAppDelegate?.setRoot()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil))
        {
            return false;
        }
        if textField.isEqual(textFldEmail)
        {
            if (textField.text?.characters.count)! >= 36 && range.length == 0
            {
                return false
            }
        }
        else  if textField.isEqual(textFldPassword)
        {
            if (textField.text?.characters.count)! >= 20 && range.length == 0
            {
                return false
            }
        }
        return true
    }
    
}
