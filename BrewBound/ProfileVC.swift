//
//  ProfileVC.swift
//  BrewBound
//
//  Created by Apple on 30/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ProfileVC: BaseVC {
    var arrayOfImages = NSArray()
    
    @IBOutlet weak var tablevw: UITableView!
    @IBOutlet weak var lblTotalOrder: UILabel!
    @IBOutlet weak var lblRewardPoint: UILabel!
    @IBOutlet weak var imgViewUser: UIImageView!
    var dicData = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       if let temp = UserDefaults.standard.value(forKey: "profileData") as? Data {
            if let dictionary: NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: temp) as? NSDictionary {
                dicData = dictionary ;
            }
        }

        imgViewUser.clipsToBounds=true
        imgViewUser.layer.cornerRadius=imgViewUser.frame.size.width/2
        
        arrayOfImages = [["image": #imageLiteral(resourceName: "userProfile"), "placeholder":"Username"],
                         ["image": #imageLiteral(resourceName: "Email a friend"), "placeholder":"Email"],
                         ["image": #imageLiteral(resourceName: "phoneNumber"), "placeholder":"Phone number"],
                         ["image": #imageLiteral(resourceName: "password"), "placeholder":"Type new password"],
                         ["image": #imageLiteral(resourceName: "password"), "placeholder":"Sign out"]]
        // self.textFields()
         self.imgViewOnRightSide()
        let request : HttpObject = ApiGenerator.getCustomerProfile(token:getToken())
        downloadData(httpRequest: request);
    }
    
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        print(object.responseObject)
        switch taskcode {
        case TASKCODES.GET_CUSTOMER_PROFILE:
            dicData = object.responseObject as! NSDictionary
            do{
                
                let dataExample: Data = NSKeyedArchiver.archivedData(withRootObject: (dicData.value(forKey: "customers") as! NSArray).object(at: 0) )

                 UserDefaults.standard.set(dataExample, forKey: "profileData")
            } catch {
                print("error getting xml string: \(error)")
            }
            print( (dicData.value(forKey: "customers") as! NSArray))

            dicData =  (dicData.value(forKey: "customers") as! NSArray).object(at: 0) as! NSDictionary
            print(dicData)
            tablevw.reloadData()
            break;

        default:
            break;
        }
        return true;
    }
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }
    
    /*func textFields()
     {
     
     var i: Int = 0
     var imagesArray = NSArray()
     imagesArray = [#imageLiteral(resourceName: "UserLogo"), #imageLiteral(resourceName: "UserLogo"), #imageLiteral(resourceName: "emailLogo"), #imageLiteral(resourceName: "PasswordLock") , #imageLiteral(resourceName: "PasswordLock"),]
     for view: UIView in .subviews
     {
     if (view is UITextField)
     {
     let textfield: UITextField? = (view as? UITextField)
     textfield?.delegate = self//Avninder
     textfield?.setImageLeftSideOnTextfield(image: imagesArray[i] as! UIImage)
     // textfield?.setImageRightSideOnTextfield(image: #imageLiteral(resourceName: "error_icon"))
     i = i + 1
     
     // textfield?.setErrorTextFld(textfield: textfield!)
     if boolIsIpad
     {
     textfield?.font =  UIFont(name:(textfield?.font?.familyName)! , size:(textfield?.font?.pointSize)!+CGFloat(4))
     }
     }*/
    
}

extension ProfileVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.backgroundColor = UIColor.clear
        let textFld = cell?.contentView.viewWithTag(1) as! UITextField
        if indexPath.row==1
        {
            textFld.text = dicData.value(forKey: "email") as? String
            textFld.keyboardType = .emailAddress
        }
        else if indexPath.row==0
        {
            textFld.text = dicData.value(forKey: "username") as? String
            textFld.isUserInteractionEnabled = false
        }
        else if indexPath.row==2
        {
            textFld.text = dicData.value(forKey: "first_name") as? String
            textFld.keyboardType = .phonePad
        }
        else if indexPath.row==3
        {
            textFld.isSecureTextEntry=true
        }
        else  if indexPath.row == 4
        {
            textFld.isEnabled=false
              textFld.text="Sign Out"
        }
         cell?.selectionStyle = .none
        textFld.setImageLeftSideOnTextfield(image:((arrayOfImages.value(forKey: "image") as! NSArray).object(at: indexPath.row) as! UIImage))
        textFld.setImageRightSideOnTextfield(image: #imageLiteral(resourceName: "About"))
        textFld.placeholder=(arrayOfImages.value(forKey: "placeholder") as! NSArray).object(at: indexPath.row) as? String
        
        return cell!
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 4
        {
           UserDefaults.standard.removeObject(forKey: "userData")
            UserDefaults.standard.removeObject(forKey: "profileData")
            let ownProfile = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(ownProfile, animated: false)
        }
    }
}
