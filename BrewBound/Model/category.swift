//
//  category.swift
//  BrewBound
//
//  Created by Rahul Mehndiratta on 8/15/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import ObjectMapper
class category : BaseMapperModel{
    
    var access_Token : String?
    var tokenType : String?
    var expiresIn : String?
    var refreshToken : String?
    
    required init?(map: Map){
        super.init(map: map)}
    
    override func mapping(map: Map){
        super.mapping(map: map)
        access_Token <- map["access_token"]
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
        refreshToken <- map["refresh_token"]
        
        
}
}
