//
//  ExploreVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import SDWebImage

class ExploreVC: BaseCollectionVC {
    @IBOutlet weak var objCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let request : HttpObject = ApiGenerator.EXPLORE(pageNumber: "1", pageSize: "10", token: getToken())
        //arrayMenuData = ["Beer","Ready to Drink", "Wine", "Special","Spirits","Extra"]
        // Do any additional setup after loading the view.
        downloadData(httpRequest: request);
    }
    
    override func getNibNamesToRegister() -> [String]? {
        return ["ExploreCVC"]
    }
    
    override func getCollectionView() -> UICollectionView? {
        return self.objCollectionView
    }
    
    override func getTitleString() -> String {
        return "Explore"
    }
    
    override func getCellHeight(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> CGFloat {
        return 170;
    }
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        switch taskcode {
        case TASKCODES.EXPLORE:
            let response = object.responseObject as! ExploreResponse;
            if let items = response.exploreItems{
                for item in items{
                    list.append(item)
                }
            }
            getCollectionView()?.reloadData()
            break;
            
            
        default:
            break;
        }
        return flag;
    }
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




