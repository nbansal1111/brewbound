//
//  RegisterVC.swift
//  BrewBound
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class RegisterVC: BaseVC,UITextFieldDelegate {
    
    @IBOutlet weak var textFldConfirmPass: UITextField!
    @IBOutlet weak var textFldPassword: UITextField!
    @IBOutlet weak var textFldMobileNo: UITextField!
    @IBOutlet weak var textFldEmail: UITextField!
    @IBOutlet weak var textFldName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func joinClicked(_ sender: Any) {
        let objAppDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        objAppDelegate?.setRoot()
    }
    
    

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        for objSubView in self.view.subviews
        {
            if (objSubView is UITextField)
            {
                let textFld: UITextField? = (objSubView as? UITextField)
                textFld?.delegate=self
                let objSpacerView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: (textFld?.frame.size.height)!))
                textFld?.leftView=objSpacerView
                textFld?.leftViewMode=UITextFieldViewMode.always
                textFld?.clipsToBounds=true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil))
        {
            return false;
        }
        if textField.isEqual(textFldName)
        {
            
            if (textField.text?.characters.count)! >= 20 && range.length == 0
            {
                return false
            }
            let allowedCharacters = CharacterSet.letters
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        else  if textField.isEqual(textFldEmail)
        {
            if (textField.text?.characters.count)! >= 36 && range.length == 0
            {
                return false
            }
        }
        else  if textField.isEqual(textFldMobileNo)
        {
            if (textField.text?.characters.count)! >= 11 && range.length == 0
            {
                return false
            }
        }
        else  if textField.isEqual(textFldPassword) || textField.isEqual(textFldConfirmPass)
        {
            if (textField.text?.characters.count)! >= 20 && range.length == 0
            {
                return false
            }
        }
        return true
    }

    @IBAction func alreadyAccountClicked(_ sender: Any) {
        backToView()
    }
    @IBAction func crossBtnClicked(_ sender: Any) {
        backToView()
    }
    
}


