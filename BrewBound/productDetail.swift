//
//  productDetail.swift
//  BrewBound
//
//  Created by Rahul.Mehndiratta on 8/17/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//


import Foundation
import ObjectMapper

class productDetail : BaseMapperModel{
    var data : [product]?
    
    required init?(map: Map){
        super.init(map: map)}
    
    override func mapping(map: Map){
        super.mapping(map: map)
        self.data <- map["products"]
    } 
}

class products : BaseMapperModel{
    var id : String!
    var visible_individually : Bool!
    var name : String!
    var short_description : String!
    var full_description : String!
    var show_on_home_page : Bool!
    var meta_keywords : String!
    var meta_description : String!
    var meta_title : String!
    var allow_customer_reviews : Bool!
    var approved_rating_sum : Int!
    var not_approved_rating_sum : Int!
    var approved_total_reviews : Int!
    var not_approved_total_reviews : Int!
    var sku : String!
    var manufacturer_part_number : String!
    var gtin : String!
    var is_gift_card : Bool!
    var require_other_products : Bool!
    var automatically_add_required_products : Bool!
    var is_download : Bool!
    var unlimited_downloads : Bool!
    var max_number_of_downloads : Int!
    var download_expiration_days : String!
    var has_sample_download : Bool!
    var has_user_agreement : Bool!
    var is_recurring : Bool!
    var recurring_cycle_length : Int!
    var recurring_total_cycles : Int!
    var is_rental : Bool!
    var rental_price_length : Int!
    var is_ship_enabled : Bool!
    var is_free_shipping : Bool!
    var ship_separately : Bool!
    var additional_shipping_charge : Int!
    var is_tax_exempt : Bool!
    var is_telecommunications_or_broadcasting_or_electronic_services : Bool!
    var use_multiple_warehouses : Bool!
    var stock_quantity : Int!
    var display_stock_availability : Bool!
    var display_stock_quantity : Bool!
    var min_stock_quantity : Int!
    var notify_admin_for_quantity_below : Int!
    var allow_back_in_stock_subscriptions : Bool!
    var order_minimum_quantity : Int!
    var order_maximum_quantity : Int!
    var allowed_quantities : String!
    var allow_adding_only_existing_attribute_combinations : Bool!
    var disable_buy_button : Bool!
    var disable_wishlist_button : Bool!
    var available_for_pre_order : Bool!
    var pre_order_availability_start_date_time_utc : String!
    var call_for_price : Bool!
    var price : Double!
    var old_price : Double!
    var product_cost : Int!
    var special_price : String!
    var special_price_start_date_time_utc : String!
    var special_price_end_date_time_utc : String!
    var customer_enters_price : Bool!
    var minimum_customer_entered_price : Int!
    var maximum_customer_entered_price : Int!
    var baseprice_enabled : Bool!
    var baseprice_amount : Int!
    var baseprice_base_amount : Int!
    var has_tier_prices : Bool!
    var has_discounts_applied : Bool!
    var weight : Int!
    var length : Int!
    var width : Int!
    var height : Int!
    var available_start_date_time_utc : String!
    var available_end_date_time_utc : String!
    var display_order : Int!
    var published : Bool!
    var deleted : Bool!
    var created_on_utc : String!
    var updated_on_utc : String!
    var product_type : String!
    var parent_grouped_product_id : Int!
    var role_ids : [String]!
    var discount_ids : [String]!
    var store_ids : [String]!
    var manufacturer_ids : [String]!
    var images : image?
    var attributes : [String]!
    var associated_product_ids : [String]!
    var tags : [String]!
    var vendor_id : Int!
    var se_name : String!

    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map){
        super.mapping(map: map)
        
        
        id <- map["id"]
        name <- map["name"]
        full_description <- map["full_description"]
        short_description <- map["short_description"]
        visible_individually <- map["visible_individually"]
        show_on_home_page <- map["show_on_home_page"]
        meta_keywords <- map["meta_keywords"]
        meta_description <- map["meta_description"]
        meta_title <- map["meta_title"]
        meta_keywords <- map["meta_keywords"]
        meta_description <- map["meta_description"]
        meta_title <- map["meta_title"]
        allow_customer_reviews <- map["allow_customer_reviews"]
        approved_rating_sum <- map["approved_rating_sum"]
        not_approved_rating_sum <- map["not_approved_rating_sum"]
        approved_total_reviews <- map["approved_total_reviews"]
        not_approved_total_reviews <- map["not_approved_total_reviews"]
        show_on_home_page <- map["show_on_home_page"]
        sku <- map["sku"]
        manufacturer_part_number <- map["manufacturer_part_number"]
        gtin <- map["gtin"]
        is_gift_card <- map["is_gift_card"]
        require_other_products <- map["require_other_products"]
        automatically_add_required_products <- map["automatically_add_required_products"]
        is_download <- map["is_download"]
        unlimited_downloads <- map["unlimited_downloads"]
        max_number_of_downloads <- map["max_number_of_downloads"]
        download_expiration_days <- map["download_expiration_days"]
        has_sample_download <- map["has_sample_download"]
        has_user_agreement <- map["has_user_agreement"]
        is_recurring <- map["is_recurring"]
        recurring_cycle_length <- map["recurring_cycle_length"]
        recurring_total_cycles <- map["recurring_total_cycles"]
        is_rental <- map["is_rental"]
        rental_price_length <- map["rental_price_length"]
        is_ship_enabled <- map["is_ship_enabled"]
        is_free_shipping <- map["is_free_shipping"]
        ship_separately <- map["ship_separately"]
        additional_shipping_charge <- map["additional_shipping_charge"]
        is_tax_exempt <- map["is_tax_exempt"]
        is_telecommunications_or_broadcasting_or_electronic_services <- map["is_telecommunications_or_broadcasting_or_electronic_services"]
        use_multiple_warehouses <- map["use_multiple_warehouses"]
        stock_quantity <- map["stock_quantity"]
        display_stock_availability <- map["display_stock_availability"]
        display_stock_quantity <- map["display_stock_quantity"]
        min_stock_quantity <- map["min_stock_quantity"]
        notify_admin_for_quantity_below <- map["notify_admin_for_quantity_below"]
        allow_back_in_stock_subscriptions <- map["allow_back_in_stock_subscriptions"]
        order_minimum_quantity <- map["order_minimum_quantity"]
        order_maximum_quantity <- map["order_maximum_quantity"]
        allowed_quantities <- map["allowed_quantities"]
        allow_adding_only_existing_attribute_combinations <- map["allow_adding_only_existing_attribute_combinations"]
        disable_buy_button <- map["disable_buy_button"]
        disable_wishlist_button <- map["disable_wishlist_button"]
        available_for_pre_order <- map["available_for_pre_order"]
        pre_order_availability_start_date_time_utc <- map["pre_order_availability_start_date_time_utc"]
        call_for_price <- map["call_for_price"]
        price <- map["price"]
        old_price <- map["old_price"]
        product_cost <- map["product_cost"]
        special_price <- map["special_price"]
        special_price_start_date_time_utc <- map["special_price_start_date_time_utc"]
        special_price_end_date_time_utc <- map["special_price_end_date_time_utc"]
        special_price_end_date_time_utc <- map["special_price_end_date_time_utc"]
        customer_enters_price <- map["customer_enters_price"]
        minimum_customer_entered_price <- map["minimum_customer_entered_price"]
        maximum_customer_entered_price <- map["maximum_customer_entered_price"]
        baseprice_enabled <- map["baseprice_enabled"]
        baseprice_amount <- map["baseprice_amount"]
        baseprice_base_amount <- map["baseprice_base_amount"]
        has_tier_prices <- map["has_tier_prices"]
        has_discounts_applied <- map["has_discounts_applied"]
        weight <- map["weight"]
        length <- map["length"]
        width <- map["width"]
        height <- map["height"]
        available_start_date_time_utc <- map["available_start_date_time_utc"]
        available_end_date_time_utc <- map["available_end_date_time_utc"]
        display_order <- map["display_order"]
        published <- map["published"]
        deleted <- map["deleted"]
        created_on_utc <- map["created_on_utc"]
        updated_on_utc <- map["updated_on_utc"]
        product_type <- map["product_type"]
        parent_grouped_product_id <- map["parent_grouped_product_id"]
        role_ids <- map["role_ids"]
        discount_ids <- map["discount_ids"]
        store_ids <- map["store_ids"]
        manufacturer_ids <- map["manufacturer_ids"]
        attributes <- map["attributes"]
        associated_product_ids <- map["associated_product_ids"]
        tags <- map["tags"]
        vendor_id <- map["vendor_id"]
        se_name <- map["se_name"]
    }
}


