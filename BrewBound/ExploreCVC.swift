//
//  ExploreCVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ExploreCVC: BaseCVC {

    @IBOutlet weak var titleTv: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func displayData(model : ICollectionCell){
        let item = model as! blogs
        titleTv.text = item.title!
    }
    
}
