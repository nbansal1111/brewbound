//
//  BaseResponse.swift
//  Reach-Swift
//
//  Created by Bansals on 28/06/17.
//  Copyright © 2017 Kartik. All rights reserved.
//

import UIKit
import ObjectMapper
class BaseResponse: BaseMapperModel {
    var error = true
    var message : String!
    var code : String!
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        error <- map["error"];
        message <- map["message"];
        code <- map["code"];
    }
}
