//
//  JsonParser.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
class JsonParser: NSObject {

    static func parseJson(taskCode:TASKCODES, response : DataResponse<String>)->AnyObject{
        
        switch taskCode {
        case .LOGIN:
            return LoginData(JSONString: response.result.value!)!
            //return accessToken.parseResponse(response: response.result.value!);
        case .GET_CATEGORY:
            return CategoryList(JSONString: response.result.value!)!
        case .SEARCH:
            return productview(JSONString: response.result.value!)!
            
        case .GET_RECIPE:
            return RecipeResponse(JSONString: response.result.value!)!
        case .PRODUCT_ASSOCIATED:
            return productDetail(JSONString: response.result.value!)!
        case .EXPLORE:
            return ExploreResponse(JSONString: response.result.value!)!
        default :
            return parseString(taskCode: taskCode, response: response) as AnyObject
        }
//        return BaseResponse(JSONString : response.result.value!)!
    }
    
    static func parseString(taskCode:TASKCODES, response : DataResponse<String>)->[String: Any]{
        return parseToDictionary(text: response.result.value!)!
    }
    static func parseToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
