//
//  AppConstants.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

enum METHODS : Int {
    case GET
    case POST
    case PUT
    case DELETE
    case MULTIPART
    case POST_JSON
}
enum TASKCODES : Int{
    case GET_DATA
    case GET_CATEGORY
    case LOGIN
    case SEARCH
    case PRODUCT_DETAIL
    case GET_CUSTOMER_PROFILE
    case GET_CUSTOMER_ORDER
    case PRODUCT_ASSOCIATED
    case EXPLORE
    case GET_RECIPE
    case update_customers
    case favorites_list
    case shopping_cart_items
    case AddShopping_cart_items
    case AddFavorites_list
    case delete_shoppingcartitems
    case delete_favoritesList
    case ADD_USER_IMAGE
}

enum HttpResponseResult : Int{
    case SUCCESS
    case FAILURE
}

struct AppURL {
    static let BASE_URL = "http://testing.brewbound.co.nz/api/";
    
    
    static let LOGIN = "\(BASE_URL)token";
    static let CATEGORY = "\(BASE_URL)categories";
    static let SEARCH = "\(BASE_URL)productsearch?term=";
    static let PRODUCT_DETAIL = "\(BASE_URL)products/";
    static let GET_CUSTOMER_PROFILE = "\(BASE_URL)GetCustomerProfile";
    static let GET_CUSTOMER_ORDER = "\(BASE_URL)customerorders?customerId=";
    static let PRODUCT_ASSOCIATED = "\(BASE_URL)products";
    static let EXPLORE = "\(BASE_URL)getblogs?pageNumber=";
    static let GET_RECIPE = "\(BASE_URL)getrecipes";
    static let update_customers    = "\(BASE_URL)update_customers";
    static let favorites_list    = "\(BASE_URL)favorites_list/";
    static let shopping_cart_items    = "\(BASE_URL)shopping_cart_items/";
    static let AddShopping_cart_items    = "\(BASE_URL)shopping_cart_items";
    static let Addfavorites_list    = "\(BASE_URL)create_favorites_list";
    static let delete_favoritesList    = "\(BASE_URL)delete_favoritesList/";
    static let delete_shoppingcartitems    = "\(BASE_URL)delete_shoppingcartitems/";




    static let OPERATION_SERVICE = "\(BASE_URL)operation/";
    static let USER_SERVICE = "\(BASE_URL)user/";
    
    static let SEND_SMS = "\(OPERATION_SERVICE)send_sms";
    static let VERIFY_NUMBER =  "\(OPERATION_SERVICE)verify_number";
    static let GET_USER = "\(USER_SERVICE)get";
    static let REGISTER_USER = "\(USER_SERVICE)register_user";
    static let REGISTER_ROUTE = "\(USER_SERVICE)register_route";
    static let GET_PATH = "\(USER_SERVICE)get_path";
    static let REGISTER_PICKING_POINT = "\(USER_SERVICE)register_picking_point";
    static let REMOVE_PICKING_POINT = "\(USER_SERVICE)remove_picking_point";
    static let ADD_USER_IMAGE = "\(USER_SERVICE)add_user_image";
    static let UPDATE_ABSENT = "\(USER_SERVICE)update_absent";
    static let ADD_USER_TO_STAND = "\(USER_SERVICE)add_user_to_stand";
    static let GET_MESSAGES = "\(USER_SERVICE)get_messages";
    static let GET_PAYMENT = "\(USER_SERVICE)get_payment";
    static let INITIATE_PAYMENT = "\(USER_SERVICE)initiate_payment";
    static let PAYMENT_COMPLETE = "\(OPERATION_SERVICE)payment_complete";
    static let TEST_GET_URL = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/d8bb95982be8a11a2308e779bb9a9707ebe42ede/sample_json";
    static let GET_TOKEN_RESPONSE = "http://otp.solbanking.com/api/OTPToken";
    
}

protocol RestDelegate: NSObjectProtocol {
    func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool
    
    func onFailure(_ paramObject: HttpResponse, forTaskCode taskCode: TASKCODES)
    func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES)
}

enum CollectionCellType : Int {
    case PRODUCT
    case HEADER_HOME
    case IMAGE_HOME_CELL
    case RECIPE
    case EXPLORE
}

protocol ICollectionCell{
    func getCellType()->CollectionCellType
}

enum CellAction : Int {
    case ADD_TO_CART_CLICKED
}

protocol DrawerItemDelegate{
    func onParentCategoryClicked(_ parentCategory : categories);
}

protocol CellActionListner {
    func onCellAction(actionType : CellAction, position : Int);
}
