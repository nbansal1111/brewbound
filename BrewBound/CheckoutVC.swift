//
//  CheckoutVC.swift
//  BrewBound
//
//  Created by Apple on 15/08/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class CheckoutVC: BaseVC
{
    @IBOutlet weak var tableViewCheckout: UITableView!
    var arrData = NSMutableArray()

    @IBOutlet weak var giftCardTF: UITextField!
    @IBOutlet weak var promotionCodeTF: UITextField!
    @IBOutlet var viewCheckout: UIView!
    @IBOutlet weak var viewTotalPrice: UIView!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewCheckout.tableFooterView = viewCheckout
      //   tableViewCheckout.sectionFooterHeight = 200
        viewTotalPrice.layer.shadowOpacity = 0.1
        viewTotalPrice.layer.borderColor=UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewTotalPrice.layer.borderWidth = 0.5
        
        for view: UIView in viewCheckout.subviews
        {
            if (view is UITextField)
            {
                let textfield: UITextField? = (view as? UITextField)
                textfield?.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
                textfield?.layer.borderWidth = 1
                textfield?.clipsToBounds=true
                
                let spacerView = UIView()
                spacerView.frame = CGRect(x: 0, y: 0, width: 10, height: (textfield?.frame.size.height)!)
                textfield?.leftView = spacerView
                textfield?.leftViewMode = UITextFieldViewMode.always
            }
        }
        let request : HttpObject = ApiGenerator.getfavList(token: getToken(), customerid: getCustomerId())
        //arrayMenuData = ["Beer","Ready to Drink", "Wine", "Special","Spirits","Extra"]
        // Do any additional setup after loading the view.
        downloadData(httpRequest: request);
        
    }
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        print(object.responseObject)
        switch taskcode {
        case TASKCODES.favorites_list:
            let arrtemp = (object.responseObject as! NSDictionary).value(forKey: "shopping_carts") as! NSArray
            arrData = arrtemp.mutableCopy() as! NSMutableArray
            
            print(arrData)
            
            
            tableViewCheckout.reloadData()
            break;
            
            
        default:
            break;
        }
        return true;
    }
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }

    @IBAction func promotionCodeApply(_ sender: Any) {
    }

    
    @IBAction func giftCardApply(_ sender: Any) {
    }


    @IBAction func btnContinueAct(_ sender: Any)
    {
        self.performSegue(withIdentifier: "shippingAddressSegue", sender: nil)
    }
}


// MARK: - Custom Class

class cellCheckout: UITableViewCell
{
    @IBOutlet weak var viewAtBack: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonRemove: UIButton!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var btnPlus: UIButton!
    
}


// MARK: - Table view data source

extension CheckoutVC: UITableViewDelegate, UITableViewDataSource
{
    func handleRemove(sender: UIButton){
        let strId = (arrData.object(at: sender.tag) as! NSDictionary).value(forKey: "product_id") as! Int
        let request : HttpObject = ApiGenerator.getShoppingCart(token: getToken(), productId:  "\(strId)")
        //arrayMenuData = ["Beer","Ready to Drink", "Wine", "Special","Spirits","Extra"]
        // Do any additional setup after loading the view.
        downloadData(httpRequest: request);
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.removefromList(indx: sender.tag)
        }
    }
    func handleadd(sender: UIButton){
        let dictemp = arrData.object(at: sender.tag) as! NSDictionary
        let dic = dictemp.mutableCopy() as! NSMutableDictionary
        var quantity = dictemp.value(forKey: "quantity") as! Int
        if quantity < 9 {
            quantity = quantity + 1
            dic.removeObject(forKey: "quantity")
dic.setValue(quantity, forKey: "quantity")
        }
        arrData.replaceObject(at: sender.tag, with: dic)
        let indexPath = IndexPath(item: sender.tag, section: 0)
        tableViewCheckout.reloadRows(at: [indexPath], with: .none)

    }
    func handleminus(sender: UIButton){
        let dictemp = arrData.object(at: sender.tag) as! NSDictionary
        let dic = dictemp.mutableCopy() as! NSMutableDictionary
        var quantity = dictemp.value(forKey: "quantity") as! Int
        if quantity > 1 {
            quantity = quantity - 1
            dic.removeObject(forKey: "quantity")
            dic.setValue(quantity, forKey: "quantity")
        }
        arrData.replaceObject(at: sender.tag, with: dic)
        let indexPath = IndexPath(item: sender.tag, section: 0)
        tableViewCheckout.reloadRows(at: [indexPath], with: .none)
    }
    func removefromList(indx:NSInteger)
    {
        arrData.removeObject(at: indx)
        tableViewCheckout.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! cellCheckout
        cell.backgroundColor = UIColor.clear
        // Adding Shadow
        cell.viewAtBack.layer.cornerRadius = 4.0
        //  cell.viewAtBack.layer.shadowOpacity = 0.5
        cell.viewAtBack.layer.borderColor=UIColor.lightGray.cgColor
        cell.viewAtBack.layer.borderWidth = 0.2
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let quantity = (arrData.object(at: indexPath.row) as!NSDictionary).value(forKey: "quantity") as! Int
        
        cell.lblQuantity.text = "\(quantity)"
        cell.buttonRemove.tag = indexPath.row
        cell.btnMinus.tag = indexPath.row
        cell.btnPlus.tag = indexPath.row

        cell.buttonRemove.addTarget(self, action:#selector(handleRemove(sender:)), for: .touchUpInside)
        cell.btnMinus.addTarget(self, action:#selector(handleminus(sender:)), for: .touchUpInside)
        cell.btnPlus.addTarget(self, action:#selector(handleadd(sender:)), for: .touchUpInside)


        cell.buttonRemove.titleLabel?.textColor=BaseVC.Color.appColor
        cell.buttonRemove.layer.borderWidth=1.0
        cell.buttonRemove.layer.borderColor=BaseVC.Color.appColor.cgColor
        return cell
    }
}
