//
//  DefaultsUtil.swift
//  Yello Bus
//
//  Created by Nitin Bansal on 02/06/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class DefaultsUtil: NSObject {
    
    static func isUserLoggedIn()->Bool{
        return UserDefaults.standard.bool(forKey: "isUserLoggedIn");
    }
    
    static func setUserLoggedIn(){
        UserDefaults.standard.set(true, forKey: "isUserLoggedIn");
    }
    static func setUserToken(_ userToken : String){
        saveString(data: userToken, key: "logintoken")
    }
    static func setType( _ type : String){
        saveString(data: type, key: "type")
    }
    
    static func logoutUser(){
        UserDefaults.standard.removeObject(forKey: "randomDeviceId")
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn");
    }
    
    static func getRandomUserId()->String{
        //        return "5cd62011-808b-46da-ae79-c587fb4c39f7";
        return UserDefaults.standard.string(forKey: "ruid")!;
    }
    
    static func getRandomIdentifier()->String{
        if let randomString = UserDefaults.standard.string(forKey: "randomDeviceId"){
            print("randomDeviceId : \(randomString)")
            return randomString;
        }else{
            let random = randomString(12);
            saveString(data: random, key: "randomDeviceId");
            print("randomDeviceId : \(random)")
            return random;
        }
    }
    
    
    static func getGCMId()->String{
        if let randomString = UserDefaults.standard.string(forKey: "gcmId"){
            print("gcmId : \(randomString)")
            return randomString;
        }else{
            let random = randomString(16);
            print("gcmId : \(random)")
            saveString(data: random, key: "gcmId");
            return random;
        }
    }
    
    static func saveString(data : String, key : String){
        UserDefaults.standard.set(data, forKey: key);
    }
    
    static func getString(key : String)->String{
        return UserDefaults.standard.string(forKey: key)!;
    }
    
    static func saveRegisteredUserResponse(data : String){
        saveString(data: data, key: "registeredUserResponse")
    }
    static func getRegisteredUserResponseString()->String{
        return getString(key: "registeredUserResponse");
    }
    
    static func saveUserData(data : String){
        saveString(data: data, key: "userData")
    }
    
//    static func saveUserData(data : NSDictionary){
//        UserDefaults.standard.string(forKey: "userData")
//    }
    
//    static func getUserData()->String{
//        if let randomString = UserDefaults.standard.string(forKey: "userData") as? NSDictionary{
//            let strtemp = randomString.value(forKey: "token_type") as! String
//            let strtemp1 = randomString.value(forKey: "access_token") as! String
//
//            return strtemp + " " + strtemp1;
//        }else{
//            return "{}";
//        }
//        
//    }
    static func getUserData()->String{
        if let randomString = UserDefaults.standard.string(forKey: "userData"){
            return randomString;
        }else{
            return "{}";
        }
        
    }
    
    static func randomString(_ length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    static func getImageUrlForUsers(contactNumber : String, fileDesc : String)->String{
        let prefix = "https://s3-ap-southeast-1.amazonaws.com/ybus-users/\(contactNumber)_image_\(fileDesc)_540.png"
        print("Image url for user : \(prefix)")
        return prefix;
    }
    
    static func getImageUrlForSchools(schoolId : String, fileDesc : String)->String{
        let prefix = "https://s3-ap-southeast-1.amazonaws.com/ybus-schools/\(schoolId)_school_\(fileDesc)_100.png";
        print("Image url for school : \(prefix)")
        return prefix;
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
    
}
