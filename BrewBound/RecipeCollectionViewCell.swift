//
//  RecipeCollectionViewCell.swift
//  BrewBound
//
//  Created by Rahul Mehndiratta on 8/26/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class RecipeCollectionViewCell: BaseCVC {
    
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var lblNewCategory: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    
    override func displayData(model: ICollectionCell) {
        
    }
}
