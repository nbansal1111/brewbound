//
//  PaymentInfoVC.swift
//  BrewBound
//
//  Created by Apple on 31/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class PaymentInfoVC: BaseVC
{
    @IBOutlet weak var viewPrice: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        for view: UIView in self.view.subviews
        {
            if (view is UITextField)
            {
                let textfield: UITextField? = (view as? UITextField)
                textfield?.delegate=self as? UITextFieldDelegate
                textfield?.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
                textfield?.layer.borderWidth = 1
                textfield?.clipsToBounds=true
                
                let spacerView = UIView()
                spacerView.frame = CGRect(x: 0, y: 0, width: 10, height: (textfield?.frame.size.height)!)
                textfield?.leftView = spacerView
                textfield?.leftViewMode = UITextFieldViewMode.always
            }
        }
        
        viewPrice.layer.shadowOpacity = 0.1
        viewPrice.layer.borderColor=UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewPrice.layer.borderWidth = 0.5
        
        self.imgViewOnRightSide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func btnPayAct(_ sender: Any)
    {
        let popup = PopupController
            .create((self.navigationController)!)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(false),
                    .scrollable(false),
                    // .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
        }
        let container = CongratulationPopUpVC.instance()
        container.closeHandler = { check in
            //            if check
            //            {
            //                let objPopUp: CongratulationPopUpVC? = self.storyboard?.instantiateViewController(withIdentifier: "CongratulationPopUpID") as? CongratulationPopUpVC
            //                self.navigationController?.pushViewController(objPopUp!, animated: false)
            //            }
            popup.dismiss()
            print("closed popup!")
        }
        popup.show(container)
    }
}
