//
//  productoverviews.swift
//  BrewBound
//
//  Created by Rahul.Mehndiratta on 8/17/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import ObjectMapper

class productview : BaseMapperModel{
    var productoverviews : [product]?
    
    required init?(map: Map){
        super.init(map: map)}
    
    override func mapping(map: Map){
        super.mapping(map: map)
        productoverviews <- map["productoverviews"]
    }
    
    //    static func parseResponse(response : String)->productview{
    //        let dataResponse : productview = productview(JSONString:response)!
    //        return dataResponse;
    //    }
    
}

class product : BaseMapperModel, ICollectionCell{
    var id : String!
    var name : String!
    var full_description : String!
    var short_description : String!
    var product_price : Double!
    var price : Double!
    var images : image?
    var imageArray : [image]?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    func getCellType() -> CollectionCellType {
        return CollectionCellType.PRODUCT
    }
    
    func getImageUrl()->String?{
        if let imageObj = images{
            return imageObj.src
        }
        if let imageList = imageArray{
            if imageList.count>0{
                return imageList[0].src
            }
        }
        return nil;
    }
    
    func getPrice()->String?{
        if product_price != nil{
            return "$ \(product_price!)"
        }else if price != nil{
            return "$ \(price!)"
        }
        return nil;
    }
    
    override func mapping(map: Map){
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        full_description <- map["full_description"]
        short_description <- map["short_description"]
        product_price <- map["product_price"]
        price <- map["price"]
        images <- map["image"]
        imageArray <- map["images"]
        
    }
}
