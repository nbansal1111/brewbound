//
//  AccountVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class AccountVC: BaseVC {
    var dictSettingData = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dictSettingData = ["PROFILE": [["title": "Rahul Mehndiratta"],
                                       ["title": "Purchases"],
                                       ["title": "Invites"]],
                           "SETTING": [["title": "My Addresses"],
                                       ["title": "Payment"]],
                           "SUPPORT":[["title": "About"],
                                      ["title": "Terms of Service"],
                                      ["title": "Privacy Policy"],
                                      ["title": "Contact Us"]]]
        
         self.imgViewOnRightSide()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AccountVC: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return dictSettingData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var sectionName = NSArray()
        var sectionTitle = NSString()
        sectionTitle = dictSettingData.allKeys[section] as! NSString
        sectionName  =  dictSettingData.value(forKey: sectionTitle as String) as! NSArray
        return sectionName.count
    }
    
    /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
     if BaseVC.DeviceType.IS_IPHONE_5
     {
     return 60.0
     }
     return 70.0
     }*/
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(tableView.frame.size.width), height: CGFloat(20)))
        let headerLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: headerView.frame.size.width, height: headerView.frame.size.height))
        headerLabel.textColor = BaseVC.Color.appColor
        headerLabel.text = dictSettingData.allKeys[section] as? String
        headerLabel.font = UIFont(name:"Lato-Bold" , size:14)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell?.backgroundColor = UIColor.clear
        }
        
            
        
        let sectionTitle = dictSettingData.allKeys[indexPath.section]
        let sectionName = dictSettingData.value(forKey: sectionTitle as! String) as! NSArray
        cell?.textLabel?.text = (sectionName.value(forKey: "title") as! NSArray).object(at: indexPath.row) as? String
        if indexPath.section==0 && indexPath.row==0
        {
             cell?.imageView?.image = #imageLiteral(resourceName: "user")
        }
        else
        {
             cell?.imageView?.image =  UIImage(named:(sectionName.value(forKey: "title") as! NSArray).object(at: indexPath.row) as! String)
        }
        if indexPath.row == 1 {
        if let temp = UserDefaults.standard.value(forKey: "profileData") as? Data
        {
            if let dictionary: NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: temp) as? NSDictionary
            {
                 cell?.textLabel?.text = dictionary?.value(forKey: "username") as? String ;
            }
        }
        }
        cell?.textLabel?.font = UIFont(name:"Lato-Regular" , size:14)
        cell?.accessoryView = UIImageView(image: UIImage(named: "About"))
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section==0
        {
            if indexPath.row==0
            {
                self.performSegue(withIdentifier: "profileSegue", sender: nil)
            }
            else if indexPath.row==1
            {
                self.performSegue(withIdentifier: "purchasesSegue", sender: nil)
            }
            else if indexPath.row==2
            {
                self.performSegue(withIdentifier: "invitesSegue", sender: nil)
            }
        }
        else if indexPath.section==1
        {
            if indexPath.row==0 {
                self.performSegue(withIdentifier: "myAddressesSegue", sender: nil)
            }
        }
    }
}
