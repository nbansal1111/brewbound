//
//  HomeVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import SDWebImage

class HomeVC: BaseCollectionVC, UITabBarControllerDelegate, RefreshListener {
    
    @IBOutlet weak var vwDropdown: UIView!
    @IBOutlet weak var spiritBtn: UIButton!
    @IBOutlet var listCollection: UICollectionView!
    @IBOutlet var vwSearchBar: UISearchBar!
    @IBOutlet weak var vwSearch: UIView!
    
    var parentCategory : categories!
    var categoryResponse : CategoryList!
    var isSearch = false;
    var strSearch = String()
    //    var list = [ICollectionCell]()
    
    @IBOutlet weak var categoryLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgViewOnRightSide()
        self.tabBarController?.delegate = self
        addImageCells()
        EventManager.getInstance().add(eventName: EventManager.CAT_CHANGE_ACTION, delegate: self);
    }
    
    override func getTitleString() -> String {
        return "Home"
    }
    
    override func getCollectionView() -> UICollectionView? {
        return self.listCollection
    }
   
    override func getNibNamesToRegister() -> [String]? {
        return ["ProductCell", "HomeHeaderCVC"]
    }
    
    func registerNib(){
        self.listCollection.register(UINib.init(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        self.listCollection.register(UINib.init(nibName: "HomeHeaderCVC", bundle: nil), forCellWithReuseIdentifier: "HomeHeaderCVC")
    }
    
    override func onLeftButtonClicked(_ sender: UIBarButtonItem) {
        AppDelegate.getInstance().setDrawerState(true);
    }
    
    @IBAction func spiritClicked(_ sender: Any) {
        
    }
    
    @IBAction func onFilterClicked(_ sender: Any) {
        
    }
    
    func getCategoryProducts(){
        getProductAssoWithCat(catId: Int(parentCategory.id!)!)
    }
    
    func addImageCells(){
        // add Header
        vwDropdown.isHidden = true;
        var header = HomeImageItem();
        header.imageName = "four"
        header.isHeader = true;
        list.append(header)
        for _ in 0..<4 {
            let item = HomeImageItem();
            item.imageName = "five"
            list.append(item)
        }
        self.listCollection.reloadData()
    }
    
    override func getCellWidth(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> CGFloat {
        switch self.getCellType(indexPath) {
        case CollectionCellType.HEADER_HOME:
            return collectionView.frame.size.width;
        default:
            return super.getCellWidth(collectionView, indexPath);
        }
    }
    
    override func getLeftButtonImageName() -> String? {
        return "add-menu-1";
    }
    
    override func getCellHeight(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> CGFloat {
        switch self.getCellType(indexPath) {
        case CollectionCellType.HEADER_HOME:
            return 140;
        case CollectionCellType.IMAGE_HOME_CELL:
            return 80;
        case CollectionCellType.PRODUCT:
            return 190;
        default:
            return super.getCellWidth(collectionView, indexPath);
        }
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        print("Home VC View Will Appear")
        super.viewWillAppear(true)
    }
    
    func getProductAssoWithCat(catId : Int){
        let request : HttpObject = ApiGenerator.productAssociated(categoryID: catId, minPrice: 0, maxPrice: 0, pageNumber: 1, pageSize: 10, token: getToken())
        downloadData(httpRequest: request);
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        if(searchBar.text?.characters.count == 0) {
            //self.view.endEditing(true)
            self.vwSearchBar.endEditing(true)
            searchBar.resignFirstResponder()
        }else{
        }
        return true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if(searchBar.text?.characters.count == 0) {
            //self.view.endEditing(true)
            self.vwSearchBar.endEditing(true)
        }else{
            searchProduct(strsearch: searchBar.text!)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        print("Selected item")
    }
    
    func onEventAction(eventName: String, data: Any) {
        let params = data as! [String : Any]
        if let parentCategory = params["parentCategory"] as? categories{
            self.parentCategory = parentCategory;
            self.categoryResponse = params["categoryListResponse"] as! CategoryList
            onCategoryChanged()
        }
    }
    
    
    
    func onCategoryChanged(){
        if parentCategory != nil{
            vwDropdown.isHidden = false;
            categoryLabel.text = parentCategory!.name;
            getCategoryProducts()
        }else{
            
        }
    }
    
    
    func searchProduct(strsearch:String){
        let request : HttpObject = ApiGenerator.getSearch(searchstring: strsearch, token: getToken())
        downloadData(httpRequest: request);
    }
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        switch taskcode {
            
        case TASKCODES.SEARCH:
            let dictemp = object.responseObject as! productview
            list.removeAll()
            for item in dictemp.productoverviews!{
                list.append(item)
            }
            self.listCollection.reloadData()
            break;
            
        case TASKCODES.PRODUCT_ASSOCIATED:
            let productRes = object.responseObject as! productDetail
            if productRes.data != nil {
                list.removeAll()
                for item in productRes.data!{
                    list.append(item)
                }
                self.listCollection.reloadData()
            }
            break;
        default:
            break;
        }
        return flag;
    }
    
    deinit {
        EventManager.getInstance().remove(eventName: EventManager.CAT_CHANGE_ACTION, delegate: self)
    }
    
}

