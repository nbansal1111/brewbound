//
//  RecipeCVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class RecipeCVC: BaseCVC {

    @IBOutlet weak var receipeAmountLabel: UILabel!
    @IBOutlet weak var receipeNameLabel: UILabel!
    @IBOutlet weak var receipeTagLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func displayData(model : ICollectionCell){
        print("Display data of Base CVC")
        addToCartBtn.isHidden = true;
        receipeTagLabel.isHidden = true;
        
        bgView.layer.borderColor=UIColor.lightGray.withAlphaComponent(0.4).cgColor
        bgView.layer.borderWidth=0.5
        bgView.clipsToBounds=true
        bgView.layer.cornerRadius=2.0
        
//        let item = model as! recipes
    }

}
