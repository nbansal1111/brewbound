//
//  AppDelegate.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import KYDrawerController
import IQKeyboardManagerSwift

var selectedIndexTab = NSInteger()

let CLIENT_ID = "a6581891-d2dd-48a4-a587-55555dcaba83"
let CLIENT_SECRET = "c996710c-47b6-4d0c-8279-f01e7a2b6c25"
let GRANTTYPE = "password"
var objDrawer: KYDrawerController!

var parentCategory : categories!
var childCategories : [categories]!


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var drawerController : KYDrawerController?
    
    static var instance : AppDelegate!
    
    
    static func getInstance()->AppDelegate{
        return instance;
    }
    
    func setDrawerState(_ open : Bool){
        if open{
           drawerController?.setDrawerState(.opened, animated: true)
        }else{
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        AppDelegate.instance = self;
        print( Date())
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        //let objBase = BaseVC()
        if(DefaultsUtil.isUserLoggedIn()){
            self.setRoot()
        }
        
        
        //
        //        UINavigationBar.appearance().isTranslucent = true
        //        UINavigationBar.appearance().setBackgroundImage(UIImage (), for: UIBarMetrics.default)
        //        UINavigationBar.appearance().shadowImage = UIImage()
        return true
    }
    
    
    
    
    
    func setRoot()
    {
        let nav = self.initializeDrawerController();
        if let w = self.window{
            print("window not null");
            w.rootViewController = nav
            w.makeKeyAndVisible()
        }
        
    }
    
    func initializeDrawerController()->KYDrawerController{
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let mainViewController   = storyboard.instantiateViewController(withIdentifier: "DemoTabBar")
        let drawerViewController = storyboard.instantiateViewController(withIdentifier: "MenuID")
        //        let drawerController     = VCUtil.getViewController(identifier: "HomeDrawer") as! HomeDrawerController
        
        
        let drawerController = KYDrawerController.init(drawerDirection: .left, drawerWidth: 280)
//        drawerController.mainViewController = UINavigationController(
//            rootViewController: mainViewController
//        )
        drawerController.mainViewController = mainViewController;
        drawerController.drawerViewController = drawerViewController
        /* Customize
         drawerController.drawerDirection = .Right
         drawerController.drawerWidth     = 200
         */
        self.drawerController = drawerController;
        return drawerController;
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

