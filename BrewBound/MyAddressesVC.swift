//
//  MyAddressesVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class MyAddressesVC: BaseVC {

    @IBOutlet weak var tableViewMyAddresses: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgViewOnRightSide()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Table view Data Source & Delegates

extension MyAddressesVC: UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! customClassMyAddresses
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Adding Shadow
         cell.viewAtBack.layer.cornerRadius = 4.0
         cell.viewAtBack.layer.shadowColor = UIColor.lightGray.cgColor
         cell.viewAtBack.layer.shadowOpacity = 0.5
         cell.viewAtBack.layer.shadowOffset = CGSize.zero
         cell.viewAtBack.layer.shadowRadius = 1.0
        
         return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(tableView.frame.size.width), height: CGFloat(30)))
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        headerView.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(tapGestureRecognizer)
        
        let headerLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: headerView.frame.size.width, height: headerView.frame.size.height-10))
        headerLabel.textColor = BaseVC.Color.appColor
        headerLabel.text = "+ Add Address"
        headerLabel.font = UIFont(name:"OpenSans-Bold" , size:14)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
      print("tapped")
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 30
    }
    
}

// MARK: - Custom Class

class customClassMyAddresses: UITableViewCell
{
    @IBOutlet weak var viewAtBack: UIView!
}
