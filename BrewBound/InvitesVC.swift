//
//  InvitesVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class InvitesVC: BaseVC {
    var arraySharingData = NSArray()
    @IBOutlet weak var tableVIewInvites: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        arraySharingData =      [["title": "Email a friend", "subTitle":"Earn a free Delivery", "shareText":"Email"],
                                 ["title": "Post to Facebook", "subTitle":"Earn a free Delivery", "shareText":"Share"],
                                 ["title": "Post to Twitter", "subTitle":"Earn a free Delivery", "shareText":"Tweet"],
                                 ["title": "Send a message", "subTitle":"Share your promo code", "shareText":"Text"]]
        self.imgViewOnRightSide()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - Custom Class

class customClassInvites: UITableViewCell
{
    @IBOutlet weak var imgViewSocialMedia: UIImageView!
    
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelSubtitle: UILabel!
    
    @IBOutlet weak var labelShare: UILabel!
}


// MARK: - Table view data source

extension InvitesVC: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arraySharingData.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return heightForRowPremium
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! customClassInvites

        cell.backgroundColor = UIColor.clear
        cell.imgViewSocialMedia.image = UIImage(named: ((arraySharingData.value(forKey: "title") as! NSArray).object(at: indexPath.row) as? String)!)
        cell.labelTitle?.text = (arraySharingData.value(forKey: "title") as! NSArray).object(at: indexPath.row) as? String
        cell.labelShare?.text = (arraySharingData.value(forKey: "shareText") as! NSArray).object(at: indexPath.row) as? String
        cell.labelSubtitle?.text = (arraySharingData.value(forKey: "subTitle") as! NSArray).object(at: indexPath.row) as? String
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.01
    }

}
