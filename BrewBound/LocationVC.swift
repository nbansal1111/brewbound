//
//  LocationVC.swift
//  BrewBound
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import GooglePlaces

class LocationVC: UIViewController {

    @IBOutlet var txtLocation: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        GMSPlacesClient.provideAPIKey("AIzaSyDvbPh9khm2bshNgjJFiu9LFdU8QBY0GYo")

        // Do any additional setup after loading the view.
    }
    
    @IBAction func continueAction(_ sender: Any) {
        let RegisterVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(RegisterVC, animated: true)

    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        for objSubView in self.view.subviews
        {
            if (objSubView is UITextField)
            {
                let textFld = objSubView as? UITextField
                textFld?.layer.borderWidth=0.6
                textFld?.clipsToBounds=true
                textFld?.layer.borderColor = UIColor.lightGray.cgColor
                
                let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: CGFloat((textFld?.frame.size.height)!)
                    , height:CGFloat((textFld?.frame.size.height)!)))
                imgView.image = #imageLiteral(resourceName: "navigation")
                imgView.contentMode = .center
                textFld?.rightView = imgView
                textFld?.rightViewMode = .always
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LocationVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        txtLocation.text! = place.name
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
               dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    
    
}
