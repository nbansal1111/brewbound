//
//  File.swift
//  BrewBound
//
//  Created by akash savediya on 16/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import ObjectMapper
class RecipeResponse : BaseMapperModel{
    
    var recipeItems : [recipes]?
    var flag : Bool?
    var mesaage : String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map){
        self.recipeItems <- map["recipes"]
    }
}

class recipes : BaseMapperModel, ICollectionCell {
    var displayorder : Int?
    var htmlcode : String?
    var isproductpicture : Bool?
    var pictureid : Int?
    var picturelink : String?
    var slideid : Int?
    var transition : Bool?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    func getCellType() -> CollectionCellType {
        return CollectionCellType.RECIPE
    }
    
    override func mapping(map: Map){
        super.mapping(map: map)
        
        displayorder <- map["displayorder"]
        htmlcode <- map["htmlcode"]
        isproductpicture <- map["isproductpicture"]
        pictureid <- map["pictureid"]
        picturelink <- map["picturelink"]
        slideid <- map["slideid"]
        transition <- map["transition"]
    }
}
