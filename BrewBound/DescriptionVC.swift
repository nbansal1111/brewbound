//
//  DescriptionVC.swift
//  BrewBound
//
//  Created by Rahul Mehndiratta on 8/26/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class DescriptionVC: UIViewController {

    @IBOutlet var collectionVw: UICollectionView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet weak var addItemToBagLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollVw.contentSize = CGSize(width: 0, height: 900)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func minusBtnClicked(_ sender: Any) {
    }
    @IBAction func addToBagClicked(_ sender: Any) {
    }
    @IBAction func plusBtnClicked(_ sender: Any) {
    }
    

}
