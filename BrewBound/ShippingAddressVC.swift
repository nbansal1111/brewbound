//
//  ShippingAddressVC.swift
//  BrewBound
//
//  Created by Apple on 31/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ShippingAddressVC: BaseVC
{
    @IBOutlet weak var viewPrice: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for view: UIView in self.view.subviews
        {
            if (view is UITextField)
            {
                let textfield: UITextField? = (view as? UITextField)
                textfield?.delegate=self as? UITextFieldDelegate
                textfield?.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
                textfield?.layer.borderWidth = 1
                textfield?.clipsToBounds=true
                
                let spacerView = UIView()
                spacerView.frame = CGRect(x: 0, y: 0, width: 10, height: (textfield?.frame.size.height)!)
                textfield?.leftView = spacerView
                textfield?.leftViewMode = UITextFieldViewMode.always
            }
        }
        self.imgViewOnRightSide()
       // viewPrice.layer.cornerRadius = 2.0
        viewPrice.layer.shadowOpacity = 0.1
        viewPrice.layer.borderColor=UIColor.lightGray.withAlphaComponent(0.5).cgColor
        viewPrice.layer.borderWidth = 0.5
    }

    @IBAction func btnContinueAct(_ sender: Any)
    {
        self.performSegue(withIdentifier: "PaymentInfoSegue", sender: nil);
    }

}
