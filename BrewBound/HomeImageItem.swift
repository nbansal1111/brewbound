//
//  HomeImageItem.swift
//  BrewBound
//
//  Created by Nitin Bansal on 19/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class HomeImageItem: NSObject, ICollectionCell {

    var imageName : String!
    var isHeader = false;
    
    override init() {
        
    }
    
    
    func getCellType() -> CollectionCellType {
        if isHeader{
            return CollectionCellType.HEADER_HOME
        }
        return CollectionCellType.IMAGE_HOME_CELL
    }
}
