//
//  FavoriteVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class FavoriteVC: BaseHomeVC {
    var arrData = NSMutableArray()
    @IBOutlet weak var tableViewFavorites: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgViewOnRightSide()
        // Do any additional setup after loading the view.
    }
    
    override func getTitleString() -> String {
        return "Favourites"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let request : HttpObject = ApiGenerator.getfavList(token: getToken(), customerid: getCustomerId())
        //arrayMenuData = ["Beer","Ready to Drink", "Wine", "Special","Spirits","Extra"]
        // Do any additional setup after loading the view.
        downloadData(httpRequest: request);
        
    }
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        print(object.responseObject)
        switch taskcode {
        case TASKCODES.favorites_list:
            let arrtemp = (object.responseObject as! NSDictionary).value(forKey: "shopping_carts") as! NSArray
            arrData = arrtemp.mutableCopy() as! NSMutableArray
            
            print(arrData)
            
            
            tableViewFavorites.reloadData()
            break;
            
            
        default:
            break;
        }
        return flag;
    }
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


// MARK: - Custom Class

class customClassFavorites: UITableViewCell
{
    @IBOutlet weak var viewAtBack: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonRemove: UIButton!
}

// MARK: - Table view data source

extension FavoriteVC: UITableViewDelegate, UITableViewDataSource
{
    func handleRemove(sender: UIButton){
        let strId = (arrData.object(at: sender.tag) as! NSDictionary).value(forKey: "product_id") as! Int
        let request : HttpObject = ApiGenerator.deleteFavList(token: getToken(), productId:  "\(strId)")
        //arrayMenuData = ["Beer","Ready to Drink", "Wine", "Special","Spirits","Extra"]
        // Do any additional setup after loading the view.
        downloadData(httpRequest: request);
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.removefromList(indx: sender.tag)
        }
    }
    func removefromList(indx:NSInteger)
    {
        arrData.removeObject(at: indx)
        tableViewFavorites.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! customClassFavorites
        cell.backgroundColor = UIColor.clear
        // Adding Shadow
        cell.viewAtBack.layer.cornerRadius = 4.0
        //  cell.viewAtBack.layer.shadowOpacity = 0.5
        cell.viewAtBack.layer.borderColor=UIColor.lightGray.cgColor
        cell.viewAtBack.layer.borderWidth = 0.2
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.buttonRemove.tag = indexPath.row
        cell.buttonRemove.addTarget(self, action:#selector(handleRemove(sender:)), for: .touchUpInside)
        
        
        cell.buttonRemove.titleLabel?.textColor=BaseVC.Color.appColor
        cell.buttonRemove.layer.borderWidth=1.0
        cell.buttonRemove.layer.borderColor=BaseVC.Color.appColor.cgColor
        return cell
    }
}
