//
//  accessToken.swift
//  BrewBound
//
//  Created by Rahul.Mehndiratta on 8/14/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class LoginResponse: BaseResponse {
    var data : LoginData?
    
    static func getInstance()->LoginData?{
        let response = DefaultsUtil.getUserData();
        let loginResponse = LoginResponse(JSONString: response);
        if !(loginResponse?.error)!{
            return (loginResponse?.data!)!;
        }
        return nil;
    }
    
    static func saveInstance(response : String)->LoginResponse{
        let loginResponse = LoginResponse(JSONString: response)
        if !(loginResponse?.error)!{
            DefaultsUtil.saveUserData(data: response);
            if let data = loginResponse?.data{
                if data.access_Token != nil{
                    DefaultsUtil.setUserToken(data.access_Token!)
                }
            }
        }
        return loginResponse!;
    }
}


class LoginData : BaseMapperModel{
    var access_Token : String?
    var tokenType : String?
    var expiresIn : String?
    var refreshToken : String?
    
    required init?(map: Map){
        super.init(map: map)}
    
    override func mapping(map: Map){
        super.mapping(map: map)
        access_Token <- map["access_token"]
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
        refreshToken <- map["refresh_token"]
    }
}

//class accessToken : BaseMapperModel{
//    
//    var access_Token : String?
//    var tokenType : String?
//    var expiresIn : String?
//    var refreshToken : String?
//    
//    required init?(map: Map){
//        super.init(map: map)}
//    
//    override func mapping(map: Map){
//        super.mapping(map: map)
//        access_Token <- map["access_token"]
//        tokenType <- map["token_type"]
//        expiresIn <- map["expires_in"]
//        refreshToken <- map["refresh_token"]
//        
//        
//    }
//
//    static func parseResponse(response : String)->accessToken{
//        let dataResponse : accessToken = accessToken(JSONString:response)!
//        print(dataResponse)
//       // DefaultsUtil.saveUserData(data: response);
//        
//        return dataResponse;
//    }
//}
