//
//  HomeHeaderCVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 19/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class HomeHeaderCVC: BaseCVC {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func displayData(model : ICollectionCell){
        let item = model as! HomeImageItem
        imageView.image = UIImage.init(named : item.imageName)
    }

}
