//
//  ReceipeDetailVC.swift
//  BrewBound
//
//  Created by Apple on 30/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ReceipeDetailVC: BaseVC {
    var arrayOfBookImages = NSArray()
    @IBOutlet weak var objScrollView: UIScrollView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.objScrollView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
        self.objScrollView.backgroundColor=UIColor.clear
        arrayOfBookImages = [#imageLiteral(resourceName: "three"),#imageLiteral(resourceName: "three"),#imageLiteral(resourceName: "three")]
        DispatchQueue.global(qos: .userInitiated).async
            {
                DispatchQueue.main.async
                    {
                        for i in 0..<self.arrayOfBookImages.count
                        {
                            let imgView = UIImageView()
                            imgView.frame = CGRect(x: CGFloat((self.objScrollView.frame.width * CGFloat(i))+20), y: CGFloat(0), width: CGFloat(self.objScrollView.frame.width-40), height: CGFloat(self.objScrollView.frame.height))
                            self.objScrollView.addSubview(imgView)
                            //imgView.contentMode = .scaleAspectFit
                            imgView.image = self.arrayOfBookImages[i] as? UIImage
                            imgView.isUserInteractionEnabled = true
                        }
                        self.objScrollView.isPagingEnabled=true
                        self.objScrollView.contentSize = CGSize(width: CGFloat(self.objScrollView.frame.width * CGFloat(self.arrayOfBookImages.count)), height: CGFloat(0))
                    }
           }
         self.imgViewOnRightSide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
