//
//  BaseCollectionVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 18/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class BaseCollectionVC: BaseHomeVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CellActionListner {
    
    var list = [ICollectionCell]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNibIdentifiers()
        // Do any additional setup after loading the view.
    }
    
    func registerNibIdentifiers(){
        if let collectionView = getCollectionView(){
            if let nibNames = getNibNamesToRegister(){
                for nib in nibNames{
                    collectionView.register(UINib.init(nibName: nib, bundle: nil), forCellWithReuseIdentifier: nib)
                }
            }
        }
    }
    
    func getNibNamesToRegister()->[String]?{
        return nil;
    }
    
    func getCollectionView()->UICollectionView?{
        return nil;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel : ICollectionCell = list[indexPath.row]
        let nibName : String = getCellNibName(item: cellModel)
        let cell = getCellFromIdentifier(nibName, indexPath) as! BaseCVC
        cell.displayData(model: cellModel)
        cell.cellActionDelegate = self;
        cell.position = indexPath.row;
        return cell;
    }
    
    func getCellType(_ indexPath : IndexPath)->CollectionCellType{
        let cellModel : ICollectionCell = list[indexPath.row]
        return cellModel.getCellType();
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width : getCellWidth(collectionView, indexPath), height : getCellHeight(collectionView, indexPath))
    }
    
    func getCellWidth(_ collectionView: UICollectionView, _ indexPath : IndexPath)->CGFloat{
        let collectionCellSize = collectionView.frame.size.width - 40;
        return collectionCellSize/2;
    }
    
    func getCellHeight(_ collectionView: UICollectionView, _ indexPath : IndexPath)->CGFloat{
        return 190
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        onCellClicked(indexPath: indexPath)
    }
    
    func getCellFromNibName(_ nibName : String)->UICollectionViewCell{
        let cell = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as! UICollectionViewCell;
        
        return cell;
    }
    
    func getCellFromIdentifier(_ identifier: String, _ indexPath : IndexPath) -> UICollectionViewCell {
        if let collectionView = getCollectionView(){
            return collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        }
        return getCellFromNibName(identifier)
    }
    
    func getCellNibName(item : ICollectionCell)->String{
        switch item.getCellType() {
        case .PRODUCT:
            return "ProductCell"
        case .HEADER_HOME, .IMAGE_HOME_CELL:
            return "HomeHeaderCVC"
        case .RECIPE:
            return "RecipeCVC"
        case .EXPLORE:
            return "ExploreCVC";
        default:
            return "";
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count;
    }
    
    override func viewWillLayoutSubviews() {
        if let cv = getCollectionView(){
            cv.contentInset = UIEdgeInsets.zero
            cv.scrollIndicatorInsets = UIEdgeInsets.zero;
        }
    }
    
    func onCellClicked(indexPath : IndexPath){
        
    }
    
    func onCellAction(actionType: CellAction, position: Int) {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
