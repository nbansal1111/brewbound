//
//  MenuVC.swift
//  BrewBound
//
//  Created by Apple on 28/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import SDWebImage
import KYDrawerController

class MenuVC: BaseVC
{
     var headerView = UIView()
//    var arrayMenuData = NSArray()
    let heightForHeader:CGFloat = 200
    @IBOutlet weak var tableViewMenu: UITableView!
    var window: UIWindow?
    var categoryListResponse : CategoryList!

    var list = [categories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request : HttpObject = ApiGenerator.getCategory(token: getToken())
        downloadData(httpRequest: request);
        tableViewMenu.tableHeaderView = headerView
    }
    
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        print(object.responseObject)
        if flag {
            switch taskcode {
            case TASKCODES.GET_CATEGORY:
                onCategoryResponseReeceived(object)
                break; 
            default:
                break;
            }
        }
        return false;
    }
    
    func onCategoryResponseReeceived(_ object : HttpResponse){
        self.categoryListResponse = object.responseObject as! CategoryList
        for item in categoryListResponse.category!{
            if item.parent_category_id == 0{
                list.append(item)
            }
        }
        
//        let dict = object.responseObject as! [String:Any]
//        categoryListResponse = CategoryList.getInstance(dict: dict)
//        list.removeAll()
//        list.append(contentsOf: CategoryList.getParentCategories(categoryListResponse))
        
        self.tableViewMenu.reloadData()
    }
    
    
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }
  
}


extension MenuVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if BaseVC.DeviceType.IS_IPHONE_5
        {
            return 60.0
        }
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell?.backgroundColor = UIColor.clear
        }
//        let url =  URL(String:(arrayMenuData[indexPath.row] as! NSDictionary).value(forKeyPath: "image.src") as! String)
//        cell?.imageView?.sd_setImage(with: URL(string: (arrayMenuData[indexPath.row] as! NSDictionary).value(forKeyPath: "image.src") as! String), placeholderImage: UIImage(named: "placeholder.png"))

//        cell?.imageView?.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""), options: SDWebImageOptions.progressiveDownload)
        cell?.textLabel?.textColor=BaseVC.Color.appColor
        cell?.textLabel?.font = UIFont(name:"Lato-Light" , size:14)
//        cell?.textLabel?.text = (arrayMenuData[indexPath.row] as! NSDictionary).value(forKey: "name") as? String
        
        cell?.textLabel?.text = list[indexPath.row].name!
        
        cell?.accessoryType = .disclosureIndicator

        cell?.accessoryView = UIImageView(image: UIImage(named: "About"))
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return heightForHeader
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
         headerView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(tableView.frame.size.width), height: heightForHeader))
        
        let imageView = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: headerView.frame.size.width, height: heightForHeader-50))
        //imageView.backgroundColor=UIColor.green
        imageView.image = #imageLiteral(resourceName: "Beer")
        headerView.addSubview(imageView)
        
        
        let buttonCross = UIButton(frame: CGRect(x: tableView.frame.maxX-50, y: CGFloat(20), width: 50, height: 50))
        buttonCross.setImage(#imageLiteral(resourceName: "Beer"), for: .normal)
        headerView.addSubview(buttonCross)
        
        let headerLabel = UILabel(frame: CGRect(x: CGFloat(0), y: imageView.frame.maxY, width: headerView.frame.size.width, height: CGFloat(20)))
        headerLabel.font = UIFont(name:"OpenSans-LightItalic" , size:20)
        headerLabel.textColor=BaseVC.Color.appColor
        headerLabel.textAlignment = .center
        headerLabel.text = "MENU"
        headerView.addSubview(headerLabel)
        
        
        let headerSubLabel = UILabel(frame: CGRect(x: CGFloat(0), y: headerLabel.frame.maxY, width: headerView.frame.size.width, height: CGFloat(20)))
        headerSubLabel.font = UIFont(name:"Lato-Light" , size:12)
        headerSubLabel.textColor=UIColor.black
        headerSubLabel.textAlignment = .center
        headerSubLabel.text = "Select your Categories"
        headerView.addSubview(headerSubLabel)
        
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row != 0 {
            let separatorView = UIView(frame: CGRect(x: CGFloat(20), y: CGFloat(0), width: CGFloat(tableView.frame.size.width-100), height: 1.0))
            separatorView.backgroundColor=BaseVC.Color.appColor.withAlphaComponent(0.2)
            cell.addSubview(separatorView)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
//        setRoot(indx: list[indexPath.row])
        var params = [String : Any]()
        params.updateValue(list[indexPath.row], forKey: "parentCategory")
        params.updateValue(self.categoryListResponse, forKey: "categoryListResponse")
        EventManager.getInstance().sendBroadcast(eventName: EventManager.CAT_CHANGE_ACTION, data: params)
        AppDelegate.getInstance().setDrawerState(false)
        print("You selected cell #\(list[indexPath.row])!")
    }
    
    
    
    func setRoot( indx: categories){
        parentCategory = indx
        selectedIndexTab=0
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var objMain: UIViewController?
        objMain = storyboard.instantiateViewController(withIdentifier: "TabBarID")
        objDrawer?.setDrawerState(.closed, animated: true)
        UserDefaults.standard.set(true, forKey: "isSearch")
        let nav = UINavigationController(rootViewController: objMain!)
        objDrawer?.mainViewController = nav
    }
    
    
}
