//
//  AvailabilityVC.swift
//  BrewBound
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class AvailabilityVC: UIViewController {

    @IBOutlet weak var btnYesOut: UIButton!
    @IBOutlet weak var btnNoOut: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        
        self.navigationController?.isNavigationBarHidden = true
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnNoAct(_ sender: Any)
    {
        btnNoOut.isSelected=true
        btnYesOut.isSelected=false
    }

    @IBAction func btnYesAct(_ sender: Any)
    {
        btnNoOut.isSelected=false
        btnYesOut.isSelected=true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
