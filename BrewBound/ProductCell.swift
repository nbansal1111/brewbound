//
//  ProductCell.swift
//  BrewBound
//
//  Created by Nitin Bansal on 18/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import SDWebImage
class ProductCell: BaseCVC {

    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var productAmounttv: UILabel!
    @IBOutlet weak var productNameTv: UILabel!
    @IBOutlet weak var productTag: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onAddButtonClicked(_ sender: Any) {
        
    }
    override func displayData(model : ICollectionCell){
        let product = model as! product
        bgView.layer.borderColor=UIColor.lightGray.withAlphaComponent(0.4).cgColor
        bgView.layer.borderWidth=0.5
        bgView.clipsToBounds=true
        bgView.layer.cornerRadius=2.0
        productNameTv.text = product.name!;
        if let url = product.getImageUrl(){
            imageView.sd_setImage(with: Foundation.URL.init(string: url))
        }
        if let price = product.getPrice(){
            productAmounttv.text = price;
        }else{
            productAmounttv.text = "";
        }
    }


}
