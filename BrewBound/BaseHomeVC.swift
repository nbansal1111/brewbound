//
//  BaseHomeVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class BaseHomeVC: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = getTitleString()
        setButtonOnRightSide()
        setLeftButton()
    }
    
    func getTitleString()->String{
        return "Brew Bound"
    }
    
    func setButtonOnRightSide(){
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(image: UIImage(named: getRightButtonImage()), style: .plain, target: self, action:#selector(self.onBackButtonClicked))
    }
    
    func onBackButtonClicked(){
        
    }
    
    func getRightButtonImage()->String{
        return "payemntBag"
    }
    
    func setLeftButton(){
        let btn1 = UIButton(type: .custom)
        if let image =  getLeftButtonImageName(){
            btn1.setImage(UIImage(named: image), for: .normal)
        }else{
            if let title = getLeftBtnTitle(){
                btn1.setTitle(title, for: .normal)
                btn1.titleLabel?.text = title;
            }
        }
        
        btn1.frame = CGRect(x: 30, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.onLeftButtonClicked(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
    }
    
    func getLeftBtnTitle()->String?{
        return "< Home"
    }
    
    
    
    func getLeftButtonImageName()->String?{
        return "previous"
    }
    
    func onLeftButtonClicked(_ sender: UIBarButtonItem){
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
