//
//  ExploreResponse.swift
//  BrewBound
//
//  Created by akash savediya on 17/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import ObjectMapper

class ExploreResponse : BaseMapperModel {
    
    var exploreItems : [blogs]?
    var flag : Bool?
    var mesaage : String?
    
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map){
        self.exploreItems <- map["blogs"]
    }
}

class blogs : BaseMapperModel, ICollectionCell {
    
    var allowcomments : Bool?
    //var blogcomments : []?
    var body : String?
    var bodyoverview : String!
    var commentcount : Int!
    var createdonutc : String!
    var enddateutc : String!
    var homepageblog : String!
    var limitedtostores : Bool!
    var metadescription : String!
    var metakeywords : String!
    var metatitle : String!
    var startdateutc : String!
    var tags : String!
    var title : String!
    
    func getCellType() -> CollectionCellType {
        return CollectionCellType.EXPLORE
    }
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map){
        super.mapping(map: map)
        
        allowcomments <- map["allowcomments"]
        //blogcomments <- map["blogcomments"]
        body <- map["body"]
        bodyoverview <- map["bodyoverview"]
        commentcount <- map["commentcount"]
        createdonutc <- map["createdonutc"]
        enddateutc <- map["enddateutc"]
        homepageblog <- map["homepageblog"]
        limitedtostores <- map["limitedtostores"]
        metadescription <- map["metadescription"]
        metakeywords <- map["metakeywords"]
        metatitle <- map["metatitle"]
        startdateutc <- map["startdateutc"]
        tags <- map["tags"]
        title <- map["title"]
    }
    
    
}
