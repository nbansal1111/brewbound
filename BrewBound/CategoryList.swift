//
//  getCategory.swift
//  BrewBound
//
//  Created by Rahul.Mehndiratta on 8/17/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import ObjectMapper
class CategoryList : BaseMapperModel{
    
    var category : [categories]?
    
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map){
        self.category <- map["categories"]
    }
    static func parseResponse(response : String)->CategoryList{
        let dataResponse : CategoryList = CategoryList(JSONString:response)!
        return dataResponse;
    }
    
//    static func getInstance(dict : [String : Any])->CategoryList{
//        let cListResponse = CategoryList(JSON: dict)
//        return cListResponse!;
//    }
    
    static func getInstance(dict : String)->CategoryList{
        let cListResponse = CategoryList(JSONString:dict)
        return cListResponse!;
    }

    
    static func getParentCategories(_ cListResponse : CategoryList)->[categories]{
        var pCategories = [categories]()
        if let cList = cListResponse.category{
            for cat in cList{
                if cat.parent_category_id == 0{
                    pCategories.append(cat)
                }
            }
        }
        return pCategories;
    }
    
    static func getChildCategories(_ cListResponse : CategoryList, pCatID : Int)->[categories]{
        var childCategories = [categories]()
        if let cList = cListResponse.category{
            for cat in cList{
                if cat.parent_category_id == pCatID{
                    childCategories.append(cat)
                }
            }
        }
        return childCategories;
    }
}


class image :BaseMapperModel
{
    var src : String?
    var attachment : String?
    required init?(map: Map){
        super.init(map: map)}
    
    override func mapping(map: Map){
        super.mapping(map: map)
        src <- map["src"]
        attachment <- map["attachment"]
        
    }
    
    
}
class categories : BaseMapperModel{
    var id : String?
    var name : String?
    var description : String?
    var category_template_id : String?
    var meta_keywords : String?
    var meta_description : String?
    var meta_title : String?
    var parent_category_id : Int!
    var page_size : String?
    var page_size_options : String?
    var price_ranges : String?
    var show_on_home_page : String?
    var include_in_top_menu : String?
    var has_discounts_applied : String?
    var published : Bool?
    var deleted : Bool?
    var display_order : String?
    var created_on_utc : String?
    var updated_on_utc : String?
    var se_name : String?
    var image: String?
    required init?(map: Map){
        super.init(map: map)}
    
    override func mapping(map: Map){
        super.mapping(map: map)
        
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        category_template_id <- map["category_template_id"]
        meta_keywords <- map["meta_keywords"]
        meta_description <- map["meta_description"]
        meta_title <- map["meta_title"]
        parent_category_id <- map["parent_category_id"]
        page_size <- map["page_size"]
        image <- map["image.src"]
        page_size_options <- map["page_size_options"]
        price_ranges <- map["price_ranges"]
        show_on_home_page <- map["show_on_home_page"]
        include_in_top_menu <- map["include_in_top_menu"]
        has_discounts_applied <- map["has_discounts_applied"]
        published <- map["published"]
        deleted <- map["deleted"]
        display_order <- map["display_order"]
        se_name <- map["se_name"]
        
        
        
    }
    
}
