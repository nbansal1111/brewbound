//
//  CongratulationPopUpVC.swift
//  BrewBound
//
//  Created by Apple on 31/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class CongratulationPopUpVC: UIViewController
{
   var closeHandler: (() -> Void)?
    
    class func instance() -> CongratulationPopUpVC
    {
        var storyboard = UIStoryboard()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CongratulationPopUpID") as! CongratulationPopUpVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnGotItAct(_ sender: Any)
    {
            closeHandler?()
    }
}

extension CongratulationPopUpVC: PopupContentViewController
{
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize
    {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
}

