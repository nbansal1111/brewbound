//
//  EventManager.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/10/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class EventManager: NSObject {
    public static let REFRESH_ACTION = "refreshAction"
    public static let NEW_MESSAGE_ACTION = "newMessageAction"
    public static let CAT_CHANGE_ACTION = "catChangeAction"
    
    var listenerSet = [String : Set<AnyHashable>]();
    private static var sharedInstance = EventManager();
    static func getInstance()->EventManager{
        return sharedInstance
    }
    func add<T>(eventName : String, delegate : T) where T : RefreshListener, T : Hashable{
        print("Add Listener")
        if var set = listenerSet[eventName]{
            set.insert(delegate)
            listenerSet.updateValue(set, forKey: eventName)
        }else{
            var set = Set<AnyHashable>();
            set.insert(delegate)
            listenerSet.updateValue(set, forKey: eventName)
        }
        
    }
    
    func remove<T>(eventName : String, delegate : T) where T : RefreshListener, T : Hashable{
        print("Remove Listener")
        if var set = listenerSet[eventName]{
            set.remove(delegate)
            listenerSet.updateValue(set, forKey: eventName)
        }else{
        }
    }
    
    
    func sendBroadcast(eventName : String){
        if let set = listenerSet[eventName]{
            for item in set{
                let delegate = item as? RefreshListener
                print("Send Bcast")
                delegate?.onEventAction!(eventName: eventName);
            }
        }
    }
    
    func sendBroadcast(eventName : String, data : Any){
        if let set = listenerSet[eventName]{
            for item in set{
                let delegate = item as? RefreshListener
                print("Send Bcast")
                delegate?.onEventAction!(eventName: eventName, data : data);
            }
        }
    }
    
    
}
@objc protocol RefreshListener {
    @objc optional func onEventAction(eventName : String)
    @objc optional func onEventAction(eventName : String, data : Any)
}
