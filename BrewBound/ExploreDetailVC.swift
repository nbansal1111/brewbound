//
//  ExploreDetailVC.swift
//  BrewBound
//
//  Created by Apple on 30/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ExploreDetailVC: BaseVC,UITextViewDelegate
{

    @IBOutlet weak var textViewComment: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        textViewComment.layer.borderWidth=0.7
        textViewComment.layer.borderColor=UIColor.darkGray.cgColor
         self.imgViewOnRightSide()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}


