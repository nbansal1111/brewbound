//
//  ApiGenerator.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ApiGenerator: NSObject {

    
    static func Login(username : String, password : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST;
        httpRequest.strUrl = AppURL.LOGIN;
        httpRequest.strtaskCode = TASKCODES.LOGIN;
        httpRequest.addParam("client_id", andValue: CLIENT_ID)
        httpRequest.addParam("client_secret", andValue: CLIENT_SECRET)
        httpRequest.addParam("grant_type", andValue: GRANTTYPE)
        httpRequest.addParam("username", andValue: username)
        httpRequest.addParam("password", andValue: password)
        httpRequest.addHeader("Content-Type", andValue: "application/x-www-form-urlencoded")
        return httpRequest;
    }
    static func getCategory(token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.CATEGORY;
        httpRequest.strtaskCode = TASKCODES.GET_CATEGORY;
        httpRequest.addHeader("Authorization", andValue:token)
        return httpRequest;
    }
    static func getSearch(searchstring : String ,token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.SEARCH + searchstring;
        httpRequest.strtaskCode = TASKCODES.SEARCH;
        httpRequest.addHeader("Authorization", andValue:token)
        return httpRequest;
    }
    static func productDetail(productId : String ,token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.PRODUCT_DETAIL + productId;
        httpRequest.strtaskCode = TASKCODES.PRODUCT_DETAIL;
        
        httpRequest.addHeader("Authorization", andValue:token)
        return httpRequest;
    }
    static func getCustomerProfile(token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.GET_CUSTOMER_PROFILE;
        httpRequest.strtaskCode = TASKCODES.GET_CUSTOMER_PROFILE;
        
        httpRequest.addHeader("Authorization", andValue:token)
        return httpRequest;
    }
    static func getCustomerOrder(customerID : String , pageNumber : String,token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.GET_CUSTOMER_ORDER + customerID + "&pageNumber=" + pageNumber + "&pageSize=10" ;
        httpRequest.strtaskCode = TASKCODES.GET_CUSTOMER_ORDER;
        
        httpRequest.addHeader("Authorization", andValue:token)
        return httpRequest;
    }
    static func productAssociated(categoryID: Int, minPrice: Int,maxPrice : Int, pageNumber : Int, pageSize: Int, token: String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST_JSON;
        httpRequest.strUrl = AppURL.PRODUCT_ASSOCIATED;
        httpRequest.strtaskCode = TASKCODES.PRODUCT_ASSOCIATED;
        httpRequest.addParam("categoryid", andValue: categoryID)
        httpRequest.addParam("minPrice", andValue: minPrice)
        httpRequest.addParam("maxPrice", andValue: maxPrice)
        httpRequest.addParam("pageNumber", andValue: pageNumber)
        httpRequest.addParam("pageSize", andValue: 10)
        httpRequest.addHeader("Content-Type", andValue: "application/json")

        return httpRequest;
    }
    
    static func EXPLORE(pageNumber : String, pageSize : String,token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.EXPLORE + pageNumber + "&pagesize=" + pageSize ;
        httpRequest.strtaskCode = TASKCODES.EXPLORE;
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        return httpRequest;
    }
    static func getRecipe(token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.GET_RECIPE ;
        httpRequest.strtaskCode = TASKCODES.GET_RECIPE;
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        return httpRequest;
    }
    static func updateProfile(id : Int,Username : String,email : String,strpassword : String,token : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST;
        httpRequest.strUrl = AppURL.update_customers;
        httpRequest.strtaskCode = TASKCODES.update_customers;
        httpRequest.addParam("id", andValue: NSNumber(value: id))
        httpRequest.addParam("Username", andValue: Username)
        httpRequest.addParam("email", andValue: email)
        httpRequest.addParam("password", andValue:strpassword)
        
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        
        return httpRequest;
    }
    static func getfavList(token : String, customerid: String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.favorites_list + customerid ;
        httpRequest.strtaskCode = TASKCODES.favorites_list;
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        
        return httpRequest;
    }
    
    static func getShoppingCart(token : String, customerid: String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.shopping_cart_items + customerid ;
        httpRequest.strtaskCode = TASKCODES.shopping_cart_items;
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        return httpRequest;
    }
    static func AddShopingCart(quantity : Int,customer_id : String,product_id : String,token : String)->HttpObject{
        let strdate = Date()
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST;
        httpRequest.strUrl = AppURL.AddShopping_cart_items;
        httpRequest.strtaskCode = TASKCODES.AddShopping_cart_items;
        httpRequest.addParam("quantity", andValue: NSNumber(value: quantity))
        httpRequest.addParam("created_on_utc", andValue:  strdate)
        httpRequest.addParam("updated_on_utc", andValue: strdate)
        httpRequest.addParam("shopping_cart_type", andValue:"ShoppingCart")
        httpRequest.addParam("product_id", andValue: product_id)
        httpRequest.addParam("customer_id", andValue:customer_id)
        
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        httpRequest.addHeader("Authorization", andValue:token)
        
        return httpRequest;
    }
    
    static func addFavList(customer_id : String,product_id : String,token : String)->HttpObject{
        let strdate = Date()
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST;
        httpRequest.strUrl = AppURL.Addfavorites_list;
        httpRequest.strtaskCode = TASKCODES.AddFavorites_list;
        httpRequest.addParam("created_on_utc", andValue:  strdate)
        httpRequest.addParam("updated_on_utc", andValue: strdate)
        httpRequest.addParam("shopping_cart_type", andValue:"wishlist")
        httpRequest.addParam("product_id", andValue: product_id)
        httpRequest.addParam("customer_id", andValue:customer_id)
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        httpRequest.addHeader("Authorization", andValue:token)
        
        return httpRequest;
    }
    static func deleteFavList(token : String, productId: String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.delete_favoritesList + productId ;
        httpRequest.strtaskCode = TASKCODES.delete_favoritesList;
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        httpRequest.addHeader("Authorization", andValue:token)
        return httpRequest;
    }
    
    static func getShoppingCart(token : String, productId: String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = AppURL.delete_shoppingcartitems + productId ;
        httpRequest.strtaskCode = TASKCODES.delete_shoppingcartitems;
        httpRequest.addHeader("Content-Type", andValue: "application/json")
        httpRequest.addHeader("Authorization", andValue:token)
        
        return httpRequest;
    }
}
