//
//  RecipesVC.swift
//  BrewBound
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class RecipesVC: BaseCollectionVC{
    
    @IBOutlet weak var objCollectionView: UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        self.objCollectionView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
        getAllRecipes()
    }
    
    override func getCellWidth(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> CGFloat {
        return 130;
    }
    
    override func getTitleString() -> String {
        return "Recipes"
    }
    
    override func getCollectionView() -> UICollectionView? {
        return self.objCollectionView
    }
    
    override func getNibNamesToRegister() -> [String]? {
        return ["RecipeCVC"]
    }
    
    func getAllRecipes(){
        let request : HttpObject = ApiGenerator.getRecipe(token: getToken())
        downloadData(httpRequest: request);
    }
    
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        print(object.responseObject)
        switch taskcode {
        case TASKCODES.GET_RECIPE:
            let recipe = object.responseObject as! RecipeResponse
            list.removeAll();
            for item in recipe.recipeItems!{
                list.append(item)
            }
            self.objCollectionView.reloadData()
            break;
        default:
            break;
        }
        return flag;
    }
    override func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        super.onPreExecute(httpRequestObject: httpRequestObject, forTaskCode: taskcode)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("You selected cell #\(indexPath.item)!")
//        self.performSegue(withIdentifier: "ReceipeDetailSegue", sender: nil)
//    }
    
}
