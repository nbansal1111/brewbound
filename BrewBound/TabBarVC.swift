//
//  TabBarVC.swift
//  BrewBound
//
//  Created by Apple on 28/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import KYDrawerController
class TabBarVC: UITabBarController, DrawerItemDelegate {
    
    func onParentCategoryClicked(_ parentCategory: categories) {
        self.selectItemWithIndex(value: 0)
    }
    
    func selectItemWithIndex(value: Int) {
        self.tabBarController?.selectedIndex = value;
//        self.tabBar(self.tabBar, didSelectItem: (self.tabBar.items as! [UITabBarItem])[value]);
    }
    
    

//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//        let btn1 = UIButton(type: .custom)
//        btn1.setImage(UIImage(named: "add-menu-1"), for: .normal)
//        btn1.frame = CGRect(x: 30, y: 0, width: 30, height: 30)
//        btn1.addTarget(self, action: #selector(TabBarVC.showSideMenu(_:)), for: .touchUpInside)
//        let item1 = UIBarButtonItem(customView: btn1)
//        self.navigationItem.leftBarButtonItem = item1
//        self.delegate=self as? UITabBarControllerDelegate
//
//        objDrawer = (navigationController?.parent as? KYDrawerController)
////        let tabBarController: UITabBarController? = (objDrawer.mainViewController.childViewControllers[0] as? UITabBarController)
////        tabBarController?.selectedIndex = selectedIndexTab
//        
//        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGray], for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: BaseVC.Color.appColor], for: .selected)
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        print("Tab : View Will Appear")
//    }
//
//    func showSideMenu(_ sender: UIBarButtonItem)
//    {
//        objDrawer.setDrawerState(.opened, animated: true)
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//
//    func tabBarController(_ tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
//        selectedIndexTab = tabBarController.selectedIndex
//        let vc: UIViewController? = (tabBarController.viewControllers?[(selectedIndexTab)])
//        if (vc is UINavigationController) {
//            (vc as? UINavigationController)?.popToRootViewController(animated: true)
//        }
//    }
//
//    
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
//        if viewController == tabBarController.viewControllers?[(selectedIndexTab)]
//        {
//            return false
//        }
//        else
//        {
//            return true
//        }
//    }

}
